# UKSTAR Guess the Interesting Fact Competition SOLVER

![UKStar-Match-and-Win-Facebook-Right-768x403.jpg](https://bitbucket.org/repo/KE8ggo/images/2621264474-UKStar-Match-and-Win-Facebook-Right-768x403.jpg)

## About
This was a done as a fun project to solve the quiz for UKSTAR competition. Suprisingly entertaining. On the end of README you can find a description of the contest, as I suspect once the winner is announced, it's going to be taken off.

## How it works
Quiz has 24 facts which you are asked to match with a subset of 29 speakers. After performance optimization, the program finishes work within 10 minutes. Keep in mind, that this may depend on the machine, but it will most certainly wildly depend on the randomized order of speakers in dropdowns.

There are some traps set up for the browser automation, so I tried to overcome these and ones that I thought may have been the case. This includes:

* Speaker list randomization in each dropdown
* Non-consistent element ID for Alan Richardson, possibly more speakers
* More speakers than facts, but each fact has exactly one match with a single Speaker
* Possibility of speaker having more than one fact about them
* Nobody matching a specific fact
* Different lists of speakers for some facts

## Running
Before you run the code, you may want to supply different participant information. To do that, edit the lines in ContestSolver.java:
```
private static final String PARTICIPANT_FIRST_NAME = "Nie";
private static final String PARTICIPANT_LAST_NAME = "Podam";
private static final String PARTICIPANT_EMAIL = "ukstar@niepodam.pl";
```
Then simply run the project by running Gradle task `run`:
```
gradlew run
```

Result:
```
(  4 %) I once took part in a national beer draught competition. /Albert Witteveen
(  8 %) I collect Super Mario Bros t-shirts! /Cassandra Leung
( 13 %) I make art quilts based on sketches of landscapes! /Isabel Evans
( 17 %) I used to DJ hardcore techno! /James Stocks
( 21 %) There was a painting of me in the Royal Academy Summer Exhibition in 2014 /Kevin Harris
( 25 %) I am an occasional Impromptu Hypnotist /Bryan Jones
( 29 %) I co-founded two successful software companies /Laurent Py
( 33 %) I wrote the character profile scripts for the FPS Game "XS" in 1996. /Alan Richardson
( 38 %) I love bicycling! /Kristian Karl
( 42 %) I take part in line dancing /Mark Harman
( 46 %) I was a film extra in a recent version of Shakespeare's "Taming of the Shrew" /Jitesh Gosai
( 50 %) I was attacked by a swan while visiting the U.K recently /Kateryna Shevchenko
( 54 %) I'm learning French at the moment /Marianne Duijst
( 58 %) I have a robot called R0B3 that I bring everywhere /Rik Marselis
( 63 %) Even though I'm short I was very good at volleyball. /Mirjana Kolarov
( 67 %) I give birth to about 100 (virtual) babies in hospitals each year to 'test' medical staff /Nathalie Van Delft
( 71 %) I organise a test conference in my home town /Marta Firlej
( 75 %) I can trace my family history back to the time of the English Civil War /Paul Collis
( 79 %) I'm a former skydiver! /Vensa Leonard
( 83 %) I am a qualified electrician! /Sue Atkins
( 88 %) I once beat a Tour de France cyclist in a folding bike race! /Stephen Janaway
( 92 %) I am a qualified Massage Therapist /Paula O'Grady
( 96 %) I was named Best young professional in the Finance IT sector in the Netherlands for 2016 /Rick Tracy
(100 %) I am a radio presenter on a Community Radio Station in Sussex /Steve Watson
Score: 100
Score: 100
Score: 100
Score: 100
Score: 100
```

## Known issues
* I've used HtmlUnitDriver, because it's much faster than MarionetteDriver for Firefox. It also doesn't cause troubles when it's getting closed.
* I count the progress myself, rather than scrap it from the result page. That's because I decided to get rid of the code that was supplying already known answers for each guess taken. So, right now, each attempt is trying to find a solution to just a single fact from the quiz.

## Competition description
> Everyone has accomplished, achieved or been part of something unique in their lives and our speakers at the inaugural UKSTAR Conference are no different.
>
> So to give you the opportunity to get to know our speakers better and even get the chance to grill them on their interesting fact in person, we are giving one lucky person a chance to win a ticket to UKSTAR.
>
> All you have to do is match the fact below to the correct speaker. Take your time thinking about each of the facts.
>
> You have five opportunities to get the as many right as you can.
>
> The person who gets 100% or the nearest to it will win a place at UKSTAR in London on the 27th-28th February.
> 
> **Terms & Conditions**
> 
> 1. Entries strictly limited to 5 per person. Only your first five entries will be counted.
> 2. Only entries with genuine email addresses will be accepted.
> 3. The Winner will be judged to be the person who achieves 100% or the highest score. If more than one person achieves the same highest score then a winner will be selected at random from the highest scores.
> 4. The winner will be notified by email.
> 5. The prize is a Two day UKSTAR ticket. Accommodation and travel costs are not included.
> 6. Prize is non-transferable.
> 7. Closing date for entries is 23:59 GMT Friday 16th December.
> 
> Best of luck!
> 
> **Interesting Fact Competition**
> 
> Match the Person to the fact:
> 
> You will be given a list of facts. All you have to do is match the fact to the speaker that you think it relates to. After you complete all the questions, hit Submit and you will be given a percentage score. Best of Luck!