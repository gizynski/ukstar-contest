package com.ukstar;

import com.ukstar.contest.Fact;
import com.ukstar.contest.Speaker;
import com.ukstar.page.ContestPage;
import com.ukstar.page.ResultPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;

public class ContestSolver {

    private static final String FAKE_FIRST_NAME = "Nie";
    private static final String FAKE_LAST_NAME = "Podam";
    private static final String FAKE_EMAIL = "ukstar@niepodam.pl";

    private static final String PARTICIPANT_FIRST_NAME = "Nie";
    private static final String PARTICIPANT_LAST_NAME = "Podam";
    private static final String PARTICIPANT_EMAIL = "ukstar@niepodam.pl";
    private static final int LIMIT_FOR_RELEVANT_SUBMISSIONS = 5;

    private static WebDriver driver;
    private static int factsSolved = 0;

    public static void main(String[] args) {
        prepareWebDriver();
        ContestPage contest = ContestPage.open(driver);
        ArrayList<Fact> facts = contest.gatherFacts();
        Set<Speaker> speakers = contest.gatherSpeakers();
        findPerfectSolution(facts, speakers);
        for (int i = 0; i < LIMIT_FOR_RELEVANT_SUBMISSIONS; i++) {
            submitPerfectSolution(facts);
        }
        driver.close();
    }

    private static void findPerfectSolution(ArrayList<Fact> facts, Set<Speaker> speakers) {
        for (Fact fact : facts) {
            findCorrectAnswer(fact, speakers);
        }
    }

    public static void submitPerfectSolution(ArrayList<Fact> facts) {
        ContestPage contest = ContestPage.open(driver);
        contest.fillParticipantInfo(PARTICIPANT_FIRST_NAME, PARTICIPANT_LAST_NAME, PARTICIPANT_EMAIL);
        for (Fact fact : facts) {
            contest.answerQuestion(fact, fact.getAuthor().getName());
        }
        ResultPage result = contest.submitForm();
        int score = result.getScore();
        System.out.println("Score: " + score);
    }

    private static Speaker findCorrectAnswer(Fact fact, Set<Speaker> speakers) {
        int score = 0;
        Speaker guess = null;
        ContestPage contest;
        Iterator<Speaker> iterator = speakers.iterator();
        while (score == 0 && iterator.hasNext()) {
            contest = ContestPage.open(driver);
            contest.fillParticipantInfo(FAKE_FIRST_NAME, FAKE_LAST_NAME, FAKE_EMAIL);
            guess = iterator.next();
            if (!contest.answerQuestion(fact, guess.getName())) {
                continue;
            }
            ResultPage result = contest.submitForm();
            score = result.getScore();
        }
        if (score > 0) {
            factsSolved++;
            fact.setAuthor(guess);
            speakers.remove(guess);
        } else {
            fact.setAuthor(new Speaker("Nobody or maybe someone had told 2 facts"));
        }
        int progress = 100 * factsSolved / 24;
        System.out.printf("(%3d %%) %s /%s%n", progress, fact.getDescription(), fact.getAuthor().getName());
        return guess;
    }

    private static void prepareWebDriver() {
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        driver = new HtmlUnitDriver();
    }

}
