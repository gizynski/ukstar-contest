package com.ukstar.contest;

public class Fact {

    private String id;
    private String description;
    private Speaker author;

    public Fact(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Speaker getAuthor() {
        return author;
    }

    public void setAuthor(Speaker author) {
        this.author = author;
    }

}
