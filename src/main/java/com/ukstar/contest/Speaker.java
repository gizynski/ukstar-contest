package com.ukstar.contest;

public class Speaker {

    private String name;

    public Speaker(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }
        if (!Speaker.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Speaker other = (Speaker) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

}
