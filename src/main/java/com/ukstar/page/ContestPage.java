package com.ukstar.page;

import com.ukstar.contest.Fact;
import com.ukstar.contest.Speaker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ContestPage {

    private static final String CONTEST_URL = "https://ukstar.eurostarsoftwaretesting.com/ukstar-guess-interesting-fact-competition/";
    private WebDriver driver;

    @FindBy(css = "li.gquiz-field")
    private List<WebElement> factFields;

    @FindBy(tagName = "option")
    private List<WebElement> allAnswers;

    @FindBy(id = "gform_submit_button_12")
    private WebElement submitButton;

    @FindBy(css = "span.name_first > input")
    private WebElement firstNameInput;
    @FindBy(css = "span.name_last > input")
    private WebElement lastNameInput;
    @FindBy(css = "div.ginput_container_email > input")
    private WebElement emailInput;

    public static ContestPage open(WebDriver driver) {
        driver.get(CONTEST_URL);
        ContestPage contest = PageFactory.initElements(driver, ContestPage.class);
        contest.setDriver(driver);
        return contest;
    }

    public ArrayList<Fact> gatherFacts() {
        ArrayList<Fact> facts = new ArrayList<>();
        for (WebElement field : factFields) {
            String factLabel = field.findElement(By.cssSelector("label")).getText();
            String factId = field.getAttribute("id");
            facts.add(new Fact(factId, factLabel));
        }
        return facts;
    }

    public Set<Speaker> gatherSpeakers() {
        Set<Speaker> speakers = new HashSet<>();
        for (WebElement answer : allAnswers) {
            String answerClass = answer.getAttribute("class");
            if (answerClass != null && answerClass.equals("gf_placeholder")) {
                continue;
            }
            String speakerName = answer.getText();
            speakers.add(new Speaker(speakerName));
        }
        return speakers;
    }

    public void fillParticipantInfo(String firstName, String lastName, String email) {
        firstNameInput.sendKeys(firstName);
        lastNameInput.sendKeys(lastName);
        emailInput.sendKeys(email);
    }

    public boolean answerQuestion(Fact fact, String name) {
        Select factSelect = new Select(driver.findElement(By.id(fact.getId())).findElement(By.tagName("select")));
        return selectOptionFromDropdown(name, factSelect);
    }

    private boolean selectOptionFromDropdown(String optionText, Select factSelect) {
        try {
            factSelect.selectByVisibleText(optionText);
        } catch (WebDriverException e) {
            return false;
        }
        return true;
    }

    public ResultPage submitForm() {
        submitButton.submit();
        ResultPage resultPage = PageFactory.initElements(driver, ResultPage.class);
        resultPage.setDriver(driver);
        return resultPage;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }
}
