package com.ukstar.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ResultPage {

    @FindBy(xpath = "//*[@id='gform_confirmation_message_12']/strong[1]")
    WebElement percentage;
    private WebDriver driver;

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public int getScore() {
        new WebDriverWait(driver, 15).until(ExpectedConditions.visibilityOf(percentage));
        String percentageString = percentage.getText();
        percentageString = percentageString.substring(0, percentageString.length() - 1);
        return Integer.parseInt(percentageString);
    }

}
